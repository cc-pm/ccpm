local args = {...}
local ccpm = {}

local project_base_url = "https://gitlab.com/api/v4/projects/"
local path_to_module_file = "/-/raw/main/module.json"
local path_to_pkg_file = "/-/raw/main/pkg.json"
local package_index = "https://gitlab.com/cc-pm/packages/-/raw/main/index.json"
local working = "/ccpm/"
local cache = working.."cache/"
local temp = working.."temp/"
local supported_files = { ".tar.gz" }
local Result = {
    SUCCESS = 0,
    FAILURE = 1,
    VERSION_MISMATCH = 2,
    INSTALLED = 3,
}


function log (color, text, inline)
    if (term.isColor()) then
        local original_color = term.getTextColor()
        term.setTextColor(color)
        if (not inline) then
            print(text)
        else
            write(text)
        end
        term.setTextColor(original_color)
    else
        if (not inline) then
            print(text)
        else
            write(text)
        end
    end
end

-- @param string
-- @param delimiter
-- @return table
function split (s, delimiter)
    local result = {}
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match)
    end
    return result
end

function split_name_and_ver (package)
    local pkg = split(package, "@")
    if (not pkg[2]) then
        pkg[2] = "stable"
    end
    return pkg
end

function ends_with (s, ending)
    return ending == "" or str:sub(-#ending) == ending
end

-- @param Name of package
-- @return Filename if in cache
function get_cache (package)
    local list = fs.list(cache)
    for _, file in pairs(list) do 
        local name = fs.getName(file)
        if name:match("(.+)%-.+") == package then
            return name
        end
    end
end

-- Checks if URL links to a supported file type
-- @param string: URL to file
-- @return string: File type
function get_type_from_url(url)
    for _, t in pairs(supported_files) do
        if (ends_with(url, t)) then
            return t
        end
    end
    printError("Unsupported file type '"..url.."'")
    error("ccpm currently does not support this file type. Supported types are:\n{ \".tar.gz\" }")
end

-- Parses index.json
-- @return table: index.json
function ccpm:parse_index_file()
    local fh, err = io.open(working.."index.json", "r")
    if (err) then
        error(err)
    end
    local json = textutils.unserializeJSON(fh:read())
    io.close(fh)
    return json
end

-- Updates index.json
-- @param string: Contents to put in file
-- @return json: Updated file in JSON
function ccpm:update_index_file(contents)
    local fh, err = io.open(working.."index.json", "w")
    if (err) then
        error(err)
    end
    local json = textutils.serializeJSON(contents)
    fh.write(json)
    io.close(fh)
    return json
end

-- Adds package to index.json
-- @param string: Name of package
-- @param string: Version of package
-- @return json: Updated file in JSON
function ccpm:add_to_index_file(contents, version)
    local index = ccpm:parse_index_file()
    index.installed[name] = version
    ccpm:update_index_file(index)
end

-- Checks if given package is installed
-- @param string: Name of package
-- @return Result: Type of Result
function ccpm:is_package_installed(package)
    local name, version = unpack(split_name_and_ver(package))
    local pkg = ccpm:parse_index_file()
    if (pkg.installed[name]) then
        if (pkg.installed[name] ~= version) then 
            return Result.VERSION_MISMATCH
        else
            return Result.INSTALLED
        end
    else
        return 0
    end
end

-- Downloads and installs a package
-- @param Repository information from GitLab
-- @return The compiled package
function ccpm:get_package(package)
    log(colors.white, "Searching for package '"..package.name.."'...")
    local request = http.get(string.format("%s%s", package.web_url, path_to_pkg_file))
    if (not request) then
        error("Could not download package")
    end
    log(colors.lime, "Found '"..package.name.."'")

    local pkg, err = load(request.readAll())
    if pkg then
        local ok, f = pcall(pkg)
        if ok then
            return f
        else
            error("Error compiling package '"..package.name.."'")
        end
    else
        error("Error compiling package '"..package.name.."'")
    end
end

-- Downloads the latest version of the master index
function ccpm:update_master_index()
    log(colors.white, "Updating master index...\n")
    local request = http.get(package_index)
    if (not request) then
        error("Error downloading master index")
    end
    local fh, err = io.open(working.."master.json", "w")
    fh:write(request.readAll())
    fh:close()
end

-- Parses master.json
-- @return table: master.json
function ccpm:parse_master_index()
    local fh, err = fs.open(working.."master.json", "rb")
    if (err) then
        error(err)
    end
    local t = textutils.unserializeJSON(fh.readAll())
    return t
end

-- Downloads the provided package tarball
-- @param string: URL of the tarball
-- @param string: Version being downloaded
-- @param string: Name of the package
function ccpm:download_package(url, version, package)
    log(colors.white, "Downloading package '"..package.."'")
    local path = cache..package.."-"..version
    local h, err, res = http.get(url, nil, true)
    if (not h) then
        log(colors.red, "Error while downloading '"..package.."'")
        log(colors.red, "Error: "..err..". HTTP Code:"..res.getResponseCode())
        error("Failed to download, exiting..")
    end
    local fh, err = io.open(path..".tar.gz", "wb")
    if (err) then
        printError("Couldn't write package tarball to disk")
        error(err)
    end
    fh:write(h.readAll())
    io.close(fh)
end

-- Extracts a package's tarball
-- @param string: Name of the package
-- @param string: Version of the package
-- @return string: Path to extracted package
function ccpm:extract_tarball(name, version)
    local tar = require("tar")
    log(colors.white, "Decompressing package..")
    local t = tar.decompress(cache..name.."-"..version.."tar.gz")
    t = tar.load(t, false, true)
    log(colors.white, "Extracting package..")
    local tmp = temp..name
    fs.makeDir(tmp)
    tar.extract(t, tmp)
    return tmp
end

-- Installs a package
-- @param string: Package name with version, e.g git@0.0.1
function ccpm:install_package(package)
    local name, version = unpack(split_name_and_ver(package))
    local pkg = ccpm:get_package(name)
    if (version == "stable") then
        version = pkg.stable()
    end

    local url = pkg.versions[version]
    fileType = get_type_from_url(url)
    local file = get_cache(name)
    if (not file) then
        ccpm:download_package(url, version, name)
    else
        log(colors.white, "Installing "..name.." from cache")
    end

    local artifact = ccpm:extract_tarball(name, version)
    local artifacts = artifact.."/"..name.."-"..version

    pkg:install(_ENV, self, artifacts, version)
    ccpm:add_to_index_file(name, version)
    fs.delete(artifact)
    log(colors.green, "Package '"..name.."' has been installed")
end

-- Removes an already installed package
-- @param string: Name of the package
function ccpm:uninstall(name)
    local packages = ccpm:parse_index_file()
    local installed = packages.installed
    local version = installed[name]
    local pkg = ccpm:get_package(name)
    pkg:uninstall(_ENV, self)
    installed[name] = nil
    ccpm:update_index_file(packages)
    log(colors.green, "Package '"..name.."' has been removed")
end

-- Search for package by name
-- @param string: Search term
function ccpm:search(name)
    log(colors.orange, "Searching for '"..name.."'...")
    local packages = ccpm:parse_index_file()
    local installed = packages.installed
    local version = installed[name]
    local master_index = ccpm:parse_master_index()
    for _, e in pairs(master_index) do
        if (e.name == name) then
            local version = installed[name]
            local i = ""
            if (version) then
                i = "(installed)"
            end
            log(colors.blue, "------------------------------------------")
            log(colors.yellow, "Name: ", true)
            log(colors.white, name.." "..i)
            log(colors.yellow, "Description: ", true)   
            log(colors.white, e.description)
            log(colors.blue, "------------------------------------------")
            return
        end
    end
    log(colors.orange, "No package '"..name.."' found")
end

-- List all packages
function ccpm:list()
    log(colors.orange, "All packages")
    local packages = ccpm:parse_index_file()
    local installed = packages.installed
    local master_index = ccpm:parse_master_index()
    for _, e in pairs(master_index) do
        local i = ""
        for n, _ in pairs(installed) do
            if(n == e.name) then i = "(installed)" end
        end
        log(colors.blue, "------------------------------------------")
        log(colors.yellow, "Name: ", true)
        log(colors.white, e.name.." "..i)
        log(colors.yellow, "Description: ", true)   
        log(colors.white, e.description)
    end
    log(colors.blue, "------------------------------------------")
    log(colors.yellow, "Total packages: ", true)
    log(colors.white, table.getn(master_index))
end

if (args[1] == "get") then
    local package = args[2]
    if (not package) then
        printError("No package specified! Correct usage:")
        printError("  ccpm get <package>")
        return
    end
    local p = ccpm:is_package_installed(package)
    local name, version = unpack(split_name_and_ver(package))
    if (p == 0) then
        ccpm:update_master_index()
        ccpm:install_package(package)
        log(colors.lime, "Finished!")
        return
    end
    if (p == Result.INSTALLED or p == Result.VERSION_MISMATCH) then
        log(colors.orange, "You already have '"..package.."' installed")
        return
    end
elseif (args[1] == "remove") then
    local package = args[2]
    local p = ccpm:is_package_installed(package)
    if (p ~= 0) then
        ccpm:uninstall(package)
        log(colors.lime, "Finished!")
        return
    end
    log(colors.orange, "Package '"..package.."' is not installed")
    return
elseif (args[1] == "search") then
    local package = args[2]
    local name, _ = unpack(split_name_and_ver(package))
    ccpm:update_master_index()
    ccpm:search(name)
    return
elseif (args[1] == "list") then
    ccpm:update_master_index()
    ccpm:list()
    return
else 
    print("Invalid usage! Correct usage:")
    print("  ccpm get <package>")
    print("  ccpm remove <package>")
    print("  ccpm search <search_term>")
end
