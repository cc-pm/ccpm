local args = {...}
local working = "/ccpm"
local startup = [=[
--               
--             ___ ___ _ __  _ __ ___  
--            / __/ __| '_ \| '_ ` _ \ 
--           | (_| (__| |_) | | | | | |
--            \___\___| .__/|_| |_| |_|
--                    | |              
--                    |_|       
--       
--          ComputerCraft Package Manager
--
-- Created by: Naomi Roberts/naomieow
-- GitLab:     https://gitlab.com/cc-pm
-- Docs:       https://gitlab.com/cc-pm/ccpm/-/blob/main/README.md
-- Issues:     https://gitlab.com/cc-pm/ccpm/-/issues
--
-- Startup file to ensure ccpm gets added to path
-- 
-- THIS FILE IS AUTO-GENERATED
-- DO NO EDIT OR DELETE
local paths = {
    "/ccpm/lib/?.lua",
    "/ccpm/lib/?",
    package.path,
}
package.path = table.concat(paths, ";")
shell.setPath(shell.path()..":".."/ccpm/bin")
]=]

function update_path()
    shell.setPath(shell.path()..":".."/ccpm/bin")
end

function create_startup_file()
    if (fs.exists("/startup") and not fs.isDir("/startup")) then
        print("Detected existing startup file. Moving it to /startup/900_existing.lua")
        fs.move("/startup.lua", "/startup/900_existing.lua")
    end
    if (fs.exists("/startup.lua")) then
        print("Detected existing startup file. Moving it to /startup/900_existing.lua")
        fs.move("/startup.lua", "/startup/900_existing.lua")
    end

    local fh, err = io.open("/startup/ccpm.lua", "w")
    if (err) then
        printError("Error creating startup file")
        error(err)
    end

    fh:write(startup)
    io.close(fh)
end

function download_deps()
    print("Downloading dependencies...")
    local deps = {
        "https://raw.githubusercontent.com/Gibbo3771/CC-Archive/master/LibDeflate.lua",
        "https://raw.githubusercontent.com/Gibbo3771/CC-Archive/master/tar.lua",
        "https://gitlab.com/cc-pm/ccpm/-/raw/main/ccpm.lua"
    }
    for i, v in ipairs(deps) do
        local filename = v:match("^.+/(.+)$")
        print("Downloading "..filename.." from "..v)
        local h, err, res = http.get(v)
        if(err) then
            print("Error downloading dependency '"..filename.."' from "..v)
            print(err)
            print(res.getResponseCode())
            error()
        end
        local path = working.."/bin/"..filename
        local fh, err = io.open(path, "w")
        if(err) then
            printError("Could not add dependency "..filename)
            error(err) 
        end
        fh:write(h.readAll())
        io.close(fh)
        print("Download complete")
    end
end

function create_index_file()
    
    local default_index_file = {installed = {}}
    local fh, err = io.open(working.."/index.json", "w")
    if(err) then
        printError("Could not create index file")
        error(err) 
    end
    fh:write(textutils.serializeJSON(default_index_file))
    io.close(fh)
end

fs.makeDir(working)
fs.makeDir(working.."/cache")
fs.makeDir(working.."/temp")
fs.makeDir(working.."/lib")

create_startup_file()
create_index_file()
update_path()
download_deps()

print("Cleaning up")
fs.delete(shell.dir().."/install.lua")
print("Successfully installed, you can now run using the 'ccpm' command")
